from flask import Flask, make_response, request
from ValidateurGenerique.FormValidator import FormValidator
from ValidateurGenerique.SchemaValidator import SchemaValidator
from ValidateurGenerique.Transformations import Transformations
import json

app = Flask(__name__)

def load_schema(file_path):
    try:
        with open(file_path, 'r') as file:
            schema = json.load(file)
            return schema
    except FileNotFoundError:
        raise ValueError(f"Error loading schema: File not found - {file_path}")
    except json.JSONDecodeError as e:
        raise ValueError(f"Error loading schema: {str(e)} - {file_path}")

file_path = 'C:\\Users\\Acer\\Desktop\\ValidateurGeneric\\schema.json'

try:
    loaded_schema = load_schema(file_path)
    print("Schema loaded successfully:", loaded_schema)
except ValueError as e:
    print("Error:", str(e))
    exit()

schema_validator = SchemaValidator(loaded_schema)
form_validator = FormValidator(loaded_schema)
transformer = Transformations(['%Y-%m-%d', '%d/%m/%Y', '%m-%d-%Y'])

@app.route('/api/validate_form', methods=['POST'])
def api_validate_form():
    try:
        data = request.get_json()
        print("Received JSON data:", data)

        
        schema_validation_result = schema_validator.validate_schema(data)

        if not schema_validation_result:
            return make_response({'status': 'error', 'error': 'Schema validation failed'}, 400)

       
        form_errors = form_validator.validate_schema(data)

        if form_errors:
            flat_errors = [{"error": str(error)} for error in form_errors]
            return make_response({'status': 'error', 'errors': flat_errors}), 400
        else:
           
            try:
                date_str = data.get('date_field')

                if date_str is not None:
                    transformed_date = transformer.transform_date(date_str)

                    if transformed_date is None:
                        raise ValueError('Invalid date format')

                    data['date_field'] = transformed_date
            except ValueError as e:
                return make_response({'status': 'error', 'error': str(e)}, 400)

            return make_response({'status': 'success', 'message': 'Form is valid', 'data': data})
    except Exception as e:
        import traceback
        traceback.print_exc()
        return make_response({'status': 'error', 'error': str(e)}, 500)

if __name__ == '__main__':
    app.run(debug=True)
